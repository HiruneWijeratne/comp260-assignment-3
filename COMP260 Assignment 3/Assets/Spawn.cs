﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

public class Spawn: MonoBehaviour
{
    public GameObject[] ghost;
    public Vector3 s_values;
    public int g_count;
    public float s_wait;
    public float g_wait;
    public float wait;
	





    

    void Start()
    {
		
       
        StartCoroutine(s_waves());
    }

    void Update()
    {
		
       
    }

    IEnumerator s_waves()
    {
        yield return new WaitForSeconds(s_wait);
        while (true )
        {
			
            for (int i = 0; i < g_count; i++)
            {
				
                GameObject ghosted = ghost[Random.Range(0, ghost.Length)];
				
					
                Vector3 s_position = new Vector3(Random.Range(-s_values.x, s_values.x), s_values.y, s_values.z);
                Quaternion s_rotation = Quaternion.identity;
                Instantiate(ghosted, s_position, s_rotation);
                yield return new WaitForSeconds(g_wait);
				
			
				
				 
            }
            yield return new WaitForSeconds(wait);
			
			
				

        
        }
    }


  
}